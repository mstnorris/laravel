<?php


namespace Websnap\Laravel\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class Websnap
{

    public function handle(Request $request, Closure $next, string $query = '')
    {
        if (!config('websnap.middlewareBypass') && !$request->isWebsnap()) {

            parse_str($query, $options);

            $options = array_merge(
                $options,
                $request->input('websnap', [])
            );

            return response()->screenshot(
                URL::full(),
                $options
            );
        }

        return $next($request);
    }
}
