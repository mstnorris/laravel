<?php


namespace Websnap\Laravel\Facades;


use Illuminate\Support\Facades\Facade;
use Psr\Http\Message\ResponseInterface;
use Websnap\Php\Client;

/**
 * @method static ResponseInterface screenshot(string $url, array $options = [])
 */
class Websnap extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Client::class;
    }
}
