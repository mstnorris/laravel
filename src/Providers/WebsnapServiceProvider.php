<?php


namespace Websnap\Laravel\Providers;

use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Websnap\Laravel\Http\Middleware\Websnap;
use Websnap\Php\Client;

class WebsnapServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/websnap.php',
            'websnap'
        );

        $this->getRouter()
            ->aliasMiddleware('websnap', Websnap::class);

        $this->registerMacros();

    }

    private function registerMacros(): void
    {
        Request::macro('isWebsnap', function(){
            return $this->hasHeader('X-Websnap-Version');
        });

        ResponseFactory::macro('screenshot', function (string $url, array $options = []) {

            $response = \Websnap\Laravel\Facades\Websnap::screenshot($url, $options);

            return $this->make(
                $response->getBody()->getContents(),
                $response->getStatusCode(),
                [
                    'Content-Type'   => $response->getHeader('Content-Type'),
                    'Content-Length' => $response->getHeader('Content-Length'),
                    'Cache-Control'  => $response->getHeader('Cache-Control')
                ]
            );
        });
    }

    private function getRouter(): Router
    {
        return $this->app->make(Router::class);
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(Client::class);

        $this->app->when(Client::class)
            ->needs('$token')
            ->give(fn() => config('websnap.token'));
    }
}
