## Installation
Install with composer: `composer require websnap/laravel`

## Getting started
1. Register for free at https://www.websnap.app and obtain a token
2. Add your token as an environment variable `WEBSNAP_TOKEN={YOUR_TOKEN}`

## Usage

### 1. Websnap Facade
Allows you to request a websnap anywhere in your code and process the response to fit your needs.
```
$response = \Websnap\Laravel\Facades\Websnap::screenshot('https://www.websnap.app');
```

The `screenshot` method will return a PSR-7 response object.

### 2. Middleware
You can add the `websnap` middleware to any route. This will request the current url through Websnap and return the screenshot as an image response.
Make sure the route is accessible from the internet else we can't snap it.

```
Route::middleware('websnap')->get('share-image', fn() => view('share-image'));
```

We can also pass the websnap options in a query string format to the middleware:
```
Route::middleware('websnap:viewport_width=1200&viewport_height=600')->get('share-image', fn() => view('share-image'));
```

As an alternative we also take the GET parameter `websnap` into consideration and forward the options to the websnap request.

```
<meta property="og:image" content="/share-image?websnap[viewport_width]=1200&websnap[viewport_height]=630">
```

For testing purposes you can bypass the middleware by setting the `WEBSNAP_MIDDLEWARE_BYPASS=true` environment variable and always force the route's return value. Eg. a view.


### 3. Response
For convenience we added a macro to the response factory to return an image response of any URL with one line of code.

```
Route::get('share-image', fn() => response()->screenshot('https://www.websnap.app'));
```

## Contact

Did you found a bug or need a certain feature? Please get in [touch](mailto:support@websnap.app) with us or [raise an issue](https://gitlab.com/ship-it-dev/websnap/laravel/-/issues/new).