<?php
return [
    'token' => env('WEBSNAP_TOKEN'),
    'middlewareBypass' => env('WEBSNAP_MIDDLEWARE_BYPASS', false)
];
